<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * English strings for pbm
 *
 * @package    pbm
 * @author     Florian Fassing
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

global $USER;

// Moodle specific
$string['modulename'] = 'PUMA/BibSonomy';
$string['pluginname'] = 'PUMA/BibSonomy';
$string['modulenameplural'] = 'PBM Instances';
$string['pluginadministration'] = 'Administration';
$string['modulename_help'] = 'PUMA/BibSonomy Plugin for Moodle. It enables you to publish literature lists from the Publication Management System PUMA or BibSonomy within a Moodle course. ';
$string['nonewmodules'] = 'No new plugins';

// General
$string['source_id'] = 'Insert user or group id';
$string['source_id_help'] = 'The name of a Bibsonomy user or group e.g..';
$string['layout'] = 'Layout';
$string['tags'] = 'Tags';
$string['tags_help'] = 'Whitespace seperated list of tags.';
$string['docs_downloadable'] = 'Provide associated documents?';
$string['docs_downloadable_help'] = 'When checked, it is possible to download attached documents.';

// Layout
$string['citation_style'] = 'Citation Style';
$string['citation_style_help'] = 'Open XML based language to describe the formatting of citations and bibliographies. See http://citationstyles.org/ for more information.';
$string['csl_language'] = 'Citation Style Language';
$string['csl_language_help'] = 'The language determines formatting of names and dates e.g.. ';

// Login
$string['auth'] = 'Select Authentication Method';
$string['auth_help'] = 'For API Key authentication you have to provide an API Key and API User in your <a title="Profile Settings" href="' . new moodle_url('/user/profile.php') . '">profile settings</a>. For OAuth you have to check the checkbox "OAuth Link" in your <a title="Profile Settings" href="' . new moodle_url('/user/profile.php') . '">profile settings</a>. After saving your changes you will be redirected to the PUMA/BibSonomy page where you have to accept the OAuth linkage (Note: Login to PUMA/BibSonomy before linking your account.). The OAuth consumer credentials have to be set by an admin in the plugin settings.';
$string['basicauth'] = 'API Key';
$string['oauth'] = 'OAuth';
$string['source_id_type'] = 'Get content from user or group?';
$string['server'] = 'Server: API Key <b>|</b> OAuth';
$string['server_description'] = 'API Key | OAuth';
$string['server_help'] = 'The default server for OAuth can be set under: Site administration -> Plugins -> Activity modules -> PUMA/BibSonomy Module. For API Key authentication the server from the <a title="Profile Settings" href="' . new moodle_url('/user/profile.php') . '">profile settings</a> is used.';
$string['login'] = 'Login';
$string['api_server'] = 'API Server';
$string['api_server_description'] = 'This server will be used when API Key is being used for authentication.';
$string['api_user'] = 'API User';
$string['api_key'] = 'API Key';
$string['api_key_help'] = 'The API Key can be found in your PUMA/BibSonomy profile settings in the "settings" tab.';
$string['oauth_connect'] = 'OAuth Link';
$string['oauth_connect_description'] = 'Link your Moodle account with your PUMA/BibSonomy account.';

// Sorting
$string['sort'] = 'Sorting and Ordering';
$string['sort_by'] = 'Sorting by';
$string['sort_order'] = 'Sorting order';
$string['year'] = 'year';
$string['title'] = 'title (alphabetical order)';
$string['author'] = 'author (alphabetical order)';
//$string['publication'] = 'publication type (define order at the bottom)';
$string['asc'] = 'ascending';
$string['desc'] = 'descending';

// Admin Settings
$string['default_server'] = 'Default server for OAuth';
$string['default_server_description'] = 'If you want to provide OAuth authentication, the selected server will be used.';
$string['consumer_key'] = 'Consumer Key';
$string['consumer_key_description'] = 'The key of a consumer for OAuth. The consumer key must first be created within the selected PUMA/BibSonomy server';
$string['consumer_secret'] = 'Consumer Secret';
$string['consumer_secret_description'] = 'The secret of a consumer for OAuth. The consumer secret must first be created within the selected PUMA/BibSonomy server';
$string['available_csl'] = 'Available CSL files:';
$string['available_csl_description'] = 'Hold down the control-left key and select multiple CSL files. The selected files will be the available styles for a user creating a PBM activity.';

$string['pbm:addinstance'] = 'Add Instance';
$string['pbm:view'] = 'View';

// View.
$string['direct_link'] = 'View this list directly on';

// Add to my puma.
$string['addtomypuma'] = 'Add to my PUMA';
$string['addtomypuma_help'] = 'With this button you can add literature to your PUMA/BibSonomy account with one click. You just have to provide authentication credentials in your <a title="Profile Settings" href="' . new moodle_url('/user/profile.php') . '">profile settings</a>. (You will also see this button if your provided credentials are wrong.)';

// Errors
$string['basic_auth_server_error'] = 'No or not valid API Key server in <a title="Profile Settings" href="' . new moodle_url('/user/profile.php') . '">profile settings</a> found.';
$string['basic_auth_error'] = 'API User, -Key or -Server is missing in your <a title="Profile Settings" href="' . new moodle_url('/user/profile.php') . '">profile settings</a>.';
$string['accesstoken_error'] = 'There seems to be no AccessToken for your profile. Have you linked (OAuth) your account to your PUMA/BibSonomy instance in the <a title="Profile Settings" href="' . new moodle_url('/user/profile.php') . '">profile settings</a> yet?';
$string['accessor_error'] = 'Failed to init accessor. Have you provided any authentication credentials in your <a title="Profile Settings" href="' . new moodle_url('/user/profile.php') . '">profile settings</a>?';
$string['restclient_error'] = "Failed to init RESTClient. Are your provided authentication credentials correct?";
$string['resources_error'] = "Sorry, no resources were found. You might want to check the activity settings.";
$string['fetch_pub_error'] = "There was an error while fetching literature which must be fixed by a programmer.";