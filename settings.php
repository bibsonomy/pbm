<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die;

global $CFG;

if ($ADMIN->fulltree) {

    $defaultserverchoices = array();

    // Get list of PUMA instances.
    $serverListUrl = 'https://www.bibsonomy.org/resources_puma/addons/list.json';
    $options = array(
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HEADER => false,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_AUTOREFERER => true,
        CURLOPT_CONNECTTIMEOUT => 120,
        CURLOPT_TIMEOUT => 120,
        CURLOPT_MAXREDIRS => 10,
    );
    $ch = curl_init($serverListUrl);
    curl_setopt_array($ch, $options);
    $responseBody = curl_exec($ch);
    curl_close($ch);
    $serverList = json_decode($responseBody);

    foreach ($serverList->server as $key => $value) {
        // Push server to array.
        $defaultserverchoices[$value->instanceUrl] = $value->instanceUrl;
    }

    // Add BibSonomy to the server list.
    $defaultserverchoices['https://www.bibsonomy.org/'] = 'https://www.bibsonomy.org/';

    /* Add the "default server" setting.
     * First parameter is $name (string): unique ascii name, either 'mysetting' for settings that in
     * config, or 'myplugin/mysetting' for ones in config_plugins.
     */
    $settings->add(new admin_setting_configselect('pbm/default_server', get_string('default_server', 'pbm'),
        get_string('default_server_description', 'pbm'), 'https://www.bibsonomy.org', $defaultserverchoices));
    /**
     * Add the "consumer key" setting.
     * PARAM_ALPHANUMEXT - expected numbers, letters only and _-.
     */
    $settings->add(new admin_setting_configtext('pbm/consumer_key', get_string('consumer_key', 'pbm'),
        get_string('consumer_key_description', 'pbm'), '', PARAM_ALPHANUMEXT));
    /**
     * Add the "consumer secret" setting.
     * PARAM_ALPHANUMEXT - expected numbers, letters only and _-.
     */
    $settings->add(new admin_setting_configtext('pbm/consumer_secret', get_string('consumer_secret', 'pbm'),
        get_string('consumer_secret_description', 'pbm'), '', PARAM_ALPHANUMEXT));

    // Get all available citation styles.
    $choices = array();
    if (($dh = opendir(realpath(dirname(__FILE__)) . '/vendor/academicpuma/styles')) !== false) {
        while (($file = readdir($dh)) !== false) {
            if (pathinfo($file, PATHINFO_EXTENSION) === "csl") {
                $keyandvalue = substr($file, 0, -4);
                $choices[$keyandvalue] = $keyandvalue;
            }
        }
        closedir($dh);
    }

    $defaults = array('bibtex', 'apa');

    asort($choices, SORT_STRING);

    $settings->add(new admin_setting_configmultiselect('pbm/available_csl_files', get_string('available_csl', 'pbm'),
        get_string('available_csl_description', 'pbm'), $defaults, $choices));
}