<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once('../mod/pbm/classes/utils/pbm_utils.php');

define('MODULE_NAME', 'pbm');

/**
 * Post installation procedure
 *
 * @see upgrade_plugins_modules()
 * @package pbm
 * @author  2014 Florian Fassing
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
function xmldb_pbm_install() {

    global $DB;

    $categorytable = 'user_info_category';
    $fieldstable = 'user_info_field';

    $categorybasicauth = get_string('modulename', MODULE_NAME). " " .get_string('api_key', MODULE_NAME);
    $categoryoauth = get_string('modulename', MODULE_NAME). " " .get_string('oauth', MODULE_NAME);

    $categoryidbasicauth = null;
    $categoryidoauth = null;

    // Add a new user profile category BasicAuth.
    if (!$DB->record_exists($categorytable, array('name' => $categorybasicauth))) {
        $category = new stdClass();
        $category->name = $categorybasicauth;
        $category->sortorder = 1;
        $categoryidbasicauth = $DB->insert_record($categorytable, $category);
    } else {
        $record = $DB->get_record($categorytable, array('name' => $categorybasicauth), 'id');
        $categoryidbasicauth = $record->id;
    }

    $defaultserverchoices = "";

    // Get list of PUMA instances.
    $serversstring = file_get_contents('https://www.bibsonomy.org/resources_puma/addons/list.json');
    $serversjson = json_decode($serversstring);

    foreach ($serversjson->server as $key => $value) {
        // Push server to array.
        $defaultserverchoices .= $value->instanceUrl . "\n";
    }

    // Add BibSonomy to the server list.
    $defaultserverchoices .= 'https://www.bibsonomy.org/';

    // Add new user profile fields.
    pbm_utils::mod_pbm_add_db_field($DB, $fieldstable, 'pbm_api_server', get_string('api_server', MODULE_NAME), 'menu',
            $categoryidbasicauth, get_string('api_server_description', MODULE_NAME), 1, $defaultserverchoices);

    pbm_utils::mod_pbm_add_db_field($DB, $fieldstable, 'pbm_api_user', get_string('api_user', MODULE_NAME), 'text',
            $categoryidbasicauth, get_string('source_id_help', MODULE_NAME), 1, 30, 50);
    pbm_utils::mod_pbm_add_db_field($DB, $fieldstable, 'pbm_api_key', get_string('api_key', MODULE_NAME), 'text',
            $categoryidbasicauth, get_string('api_key_help', MODULE_NAME), 1, 30, 50);

    // Add a new user profile category OAuth.
    if (!$DB->record_exists($categorytable, array('name' => $categoryoauth))) {
        $category = new stdClass();
        $category->name = $categoryoauth;
        $category->sortorder = 2;
        $categoryidoauth = $DB->insert_record($categorytable, $category);
    } else {
        $record = $DB->get_record($categorytable, array('name' => $categoryoauth), 'id');
        $categoryidoauth = $record->id;
    }

    pbm_utils::mod_pbm_add_db_field($DB, $fieldstable, 'pbm_oauth_connect', get_string('oauth_connect', MODULE_NAME), 'checkbox', $categoryidoauth,
            get_string('oauth_connect_description', MODULE_NAME), 1);
    pbm_utils::mod_pbm_add_db_field($DB, $fieldstable, 'pbm_access_token', 'access_token', 'text', $categoryidoauth, '', 0, 0, 0, 1);
}

/**
 * Post installation recovery procedure
 *
 * @see upgrade_plugins_modules()
 */
function xmldb_pbm_install_recovery() {
}