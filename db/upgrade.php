<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file keeps track of upgrades to the pbm module
 *
 * Sometimes, changes between versions involve alterations to database
 * structures and other major things that may break installations. The upgrade
 * function in this file will attempt to perform all the necessary actions to
 * upgrade your older installation to the current version. If there's something
 * it cannot do itself, it will tell you what you need to do.  The commands in
 * here will all be database-neutral, using the functions defined in DLL libraries.
 *
 * @package    pbm
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../mod/pbm/classes/utils/pbm_utils.php');

defined('MOODLE_INTERNAL') || die();
define('MODULE_NAME', 'pbm');

/**
 * Execute pbm upgrade from the given old version
 *
 * @param int $oldversion
 * @return bool
 */
function xmldb_pbm_upgrade($oldversion) {
    global $DB;

    $defaultserverchoices = "";

    // Get list of PUMA instances.
    $serversstring = file_get_contents('https://www.bibsonomy.org/resources_puma/addons/list.json');
    $serversjson = json_decode($serversstring);

    foreach ($serversjson->server as $key => $value) {
        // Push server to array.
        $defaultserverchoices .= $value->instanceUrl . "\n";
    }

    // Add BibSonomy to the server list.
    $defaultserverchoices .= 'https://www.bibsonomy.org/';

    $categorytable = 'user_info_category';
    $fieldstable = 'user_info_field';

    if ($oldversion <= 2015102003) {

        $condition = array('name' => get_string('modulename', MODULE_NAME));

        // Check if the old PUMA/BibSonomy category exists and rename it.
        if ($DB->record_exists($categorytable, $condition)) {

            // Add a new field "server" into the old category.
            pbm_utils::mod_pbm_add_db_field($DB, $fieldstable, 'pbm_api_server', get_string('api_server', MODULE_NAME), 'menu',
                $DB->get_field($categorytable, 'id', $condition), get_string('api_server_description', MODULE_NAME), 1, 30, 50);

            // Rename the category.
            $newname = get_string('modulename', MODULE_NAME). " " .get_string('api_key', MODULE_NAME);
            $DB->set_field($categorytable, 'name', $newname, $condition);
        }

        $categoryname = get_string('modulename', MODULE_NAME). " " .get_string('oauth', MODULE_NAME);
        $condition2 = array('name' => $categoryname);
        $categoryid = null;

        // Add a new user profile category OAuth.
        if (!$DB->record_exists($categorytable, $condition2)) {
            $category = new stdClass();
            $category->name = $categoryname;
            $category->sortorder = 2;
            $categoryid = $DB->insert_record($categorytable, $category);
        }

        // Put the existing OAuth fields into the new category.
        if ($categoryid != null) {
            $DB->set_field($fieldstable, 'categoryid', $categoryid, array('shortname' => 'pbm_oauth_connect'));
            $DB->set_field($fieldstable, 'categoryid', $categoryid, array('shortname' => 'pbm_access_token'));
        }
    }

    if ($oldversion <= 2015102007) {
        // Make the API Server field a dropdown menu and add the choices.
        $DB->set_field($fieldstable, 'datatype', 'menu', array('shortname' => 'pbm_api_server'));
        $DB->set_field($fieldstable, 'param1', $defaultserverchoices, array('shortname' => 'pbm_api_server'));

        // Add description to existing user fields.
        $DB->set_field($fieldstable, 'description', get_string('source_id_help', MODULE_NAME),
                array('shortname' => 'pbm_api_user'));
        $DB->set_field($fieldstable, 'description', get_string('api_key_help', MODULE_NAME), array('shortname' => 'pbm_api_key'));
        $DB->set_field($fieldstable, 'description', get_string('oauth_connect_description', MODULE_NAME),
                array('shortname' => 'pbm_oauth_connect'));
    }

    // The tag field needs to be renamed to not override moodles default tag field.
    if ($oldversion <= 2015102010) {
        $dbman = $DB->get_manager();
        $table = new xmldb_table('pbm');
        $field = new xmldb_field('tags', XMLDB_TYPE_CHAR);

        if ($dbman->table_exists($table)) {
            if ($dbman->field_exists($table, $field)) {
                $dbman->rename_field($table, $field, 'pbm_tags');
            }
        }
    }

    // Final return of upgrade result (true, all went good) to Moodle.
    return true;
}