PUMA/BibSonomy Module
======

PBM is the PUMA/BibSonomy plugin for [**Moodle**](https://moodle.org). It enables you to publish literature lists from the Publication Management System [PUMA](http://www.academic-puma.de "Academic Publication Management") or [BibSonomy](http://www.bibsonomy.org "Go to bibsonomy.org") within a Moodle course.

More information about PUMA and BibSonomy can be found at [www.academic-puma.de](http://www.academic-puma.de) and at [bitbucket.bibsonomy.org](http://bitbucket.bibsonomy.org)

Please note that **you need a PUMA account from the PUMA server of your institution** in order to fetch publication lists from it. Alternatively you can create a BibSonomy account for free in order to test PBM with BibSonomy.

1. What does it do?
2. For Admins
3. For Users

##1. What does it do?##

The PBM plugin enables you to publish lists of literature within a moodle course. Such lists are fetched from the Publication Management Software PUMA or the social bookmark and publication sharing system [BibSonomy](http://www.bibsonomy.org "Go to bibsonomy.org"). The lists are configured through specifying a user name, a user group, and/or a set of tags.

The style in which the bibliography is displayed on the moodle course page can be chosen from various CSL files ([Citation Style Language](http://citationstyles.org/ "Go to citationstyles.org")).

![Picture of a PBM instance](https://bytebucket.org/bibsonomy/pbm/raw/fbcba0826d3239bc8d3f7f499879354b2e9af767/pix/pbm_activity.jpg "This is how a publication list could look like.")

##2. For Admins##

### 2.1 Installation ###

The following steps are necessary to add the PBM Plugin to a running Moodle installation.

  - Download the plugin from the BitBucket repository: [https://bitbucket.org/bibsonomy/pbm](https://bitbucket.org/bibsonomy/pbm)
    or directly from Moodles plugin repository: [https://moodle.org/plugins/view.php?plugin=mod_pbm](https://moodle.org/plugins/view.php?plugin=mod_pbm)
  - Unzip the file within your mod folder of the moodle installation. (e.g. /var/www/moodle/mod/pbm)
  - Go to the Moodle website, log in as an administrator and navigate to "Site administration -> Plugins -> Plugins overview".
  - Click the button "Check for available updates" at the top of the plugins overview page. (Most of the time Moodle will inform you about database updates before the previous step, making this one obsolete.)
  - Moodle should now inform you that database updates have to be made. Confirm that.
  - You will be asked for a default server address, OAuth consumer credentials and a choice of citation styles. See 2.2 for Details.

### 2.2 Configuration ###

 ![Picture of the PBM admin settings](https://bytebucket.org/bibsonomy/pbm/raw/fbcba0826d3239bc8d3f7f499879354b2e9af767/pix/pbm_admin_settings.jpg "This is how the admin settings of PBM look like.")

 **Default server for OAuth**: If you want to provide OAuth authentication, the selected server will be used. Choose your institutional PUMA server or the URL of BibSonomy (e.g. http://puma.uni-kassel.de/ or http://www.bibsonomy.org/).

 **Comsumer-Key/Consumer-Secret**(Optional): An admin of a PUMA system can create OAuth consumer credentials in the local admin settings. They will be used to link user accounts to PUMA in order to use OAuth authentication. Alternatively you can write an E-Mail to <webmaster@bibsonomy.org> when using BibSonomy to demand OAuth consumer credentials. This is optional as it's also possible to use API Key authentication (BasicAuth).

 **Available CSL files**: Here you can choose between several citation styles which will be available when adding/editing PBM instances within courses. You can find more information about citation styles here: ([Citation Style Language](http://citationstyles.org/ "Go to citationstyles.org")).

## 3. For Users ##

### 3.1 Create a publication list in a Moodle course ###
The following steps explain how the plugin can be used by a teacher/trainer to generate literature lists on a Moodle course page.
**Note**: The teacher/trainer who creates a new PBM instance needs valid authentication credentials deposited in his/her user profile settings.
A user profile can be edited by clicking the user name in the upper right corner (default layout) and choosing "Profile". The PBM specific fields can be found under the PUMA/BibSonomy API Key and OAuth category. See 3.3 for details.

 - Turn on the editing mode and click the "Add an activity or resource" link.
 - A list with activity modules will pop up. Mark PBM and click add.
 - Now you can add information in order to specify the list of literature.

 ![Picture of a PBM configuration](https://bytebucket.org/bibsonomy/pbm/raw/fbcba0826d3239bc8d3f7f499879354b2e9af767/pix/pbm_activity_settings.jpg "This is how the configuration of a PBM instance looks like.")

 **Get content from user or group?**: Lists of literature can be fetched from a specific user or a group.

 **Insert user or group id**: The identifier for a source: e.g. a user name or group id.

 **Tags**: A white space separated list of tags.

 **Provide associated documents?**: When checked, to literature attached documents will be displayed and downloadable.

 **Select Authentication Method**: Determines which authentication method should be used when the publications being fetched from the PUMA/BibSonomy server. See 3.3 for details.

 **Server: API Key | OAuth**: Here you can see the currently chosen server for API Key authentication and OAuth authentication. The server for API Key authentication can be selected within the profile settings. The OAuth server has to be selected by an admin in the plugin settings.

 **Citation style**: The citation style determines how literature will be formatted. PBM comes with a couple of csl files. Find more at ([http://citationstyles.org/](http://citationstyles.org/ "Go to citationstyles.org")).

 **Citation style language**: Some citation styles may differ between certain languages.

### 3.2 Add to my PUMA Button ###

 When a user has provided authentication credentials (See 3.3 for details) he/she can add publications from a list to his personal list on PUMA/BibSonomy.
 This is achieved through the button at the lower right of each publication. When a publication already is in the personal list a green tick will be displayed.

### 3.3 Configuration ###

 ![Picture of the PBM user settings](https://bytebucket.org/bibsonomy/pbm/raw/fbcba0826d3239bc8d3f7f499879354b2e9af767/pix/pbm_user_settings.jpg "This is how the user settings of PBM look like.")

 **API Server**: This server will be used for API Key authentication (BasicAuth).

 **API User**: The user id of an PUMA/BibSonomy account.

 **API Key**: The API key can be found at "Settings -> Settings-Tab" in the API area when logged in at a PUMA/BibSonomy system.

 **OAuth Link**: When this field is checked and changes of the profile are saved the user will be redirected to the default OAuth server (see 2.2) to allow this user using OAuth.

 **Note** that the user has to be logged in at the PUMA/BibSonomy system **before** linking his/her account. Otherwise the redirection will not work properly and he/she needs to do the linkage again.

## 4. Changelog ##

 Version: 2018082100
 
  - Update/Adjustments to latest RestClient
  - Using cURL for requesting publication host sites now
  - Fixed bug, where publications not showing and links were broken

 Version: 2016102811

  - Fixed bug where the plugins tags field would hide Moodles own tags field.
  - Added https to all BibSonomy URLs.

 Version: 2015102009

  - OAuth authentication option in activity settings is disabled when no consumer credentials available.

 Version: 2015102008

  - Authentication credentials are now provided separately for API Key authentication (BasicAuth) and OAuth authentication.

 Version: 2015102001

  - Fixed a bug where it was not possible to bring up an OAuth linkage.

 Version: 2015102000

  - Added possibility to use OAuth. (For users when using the new add button, for teachers/trainers when publication lists are being fetched from the a list)
  - Prefered authentication method can now be selected in every single PBM instance.
  - A new button makes it easy to add publications directly from Moodle to the personal list on PUMA/BibSonomy.
  - Admins can now limit/extend the available citation styles for users creating a PBM instance.
  - As some citation styles differ between languages it's now possible to specify the language.
  - Revised readme file.