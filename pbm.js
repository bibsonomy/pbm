
function loadurl(dest, objID) {

    var xmlhttp;

    try {
        xmlhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    } catch (e) {
        console.log(e);
    }

    xmlhttp.onreadystatechange = function() {
        triggered(objID, xmlhttp);
    };

    xmlhttp.open("GET", dest);
    xmlhttp.send(null);
}

function triggered(objID, xmlhttp) {

    /*
     * 4:   request finished and response is ready
     * 200: "OK"
     */
    if ((xmlhttp.readyState === 4) && (xmlhttp.status === 200)) {
        document.getElementById(objID).innerHTML = xmlhttp.responseText;
    }
}

window.addEventListener("load", function() {
    var items = document.getElementsByClassName('pbm-bib-link');

    for (i = 0; i < items.length; i++) {
        items[i].addEventListener('click', function(event) {
            clickPubLink(event, this);
        });
    }

    // Render "add to my list" buttons.
    items = document.getElementsByClassName('pbm-add-to-my-list-wrap');

    for (i = 0; i < items.length; i++) {
        var urlToLoad = items[i].getAttribute("data-pbm") + "&intrahash=" + items[i].getAttribute("id").substr(4);
        loadurl(urlToLoad, items[i].getAttribute("id"));
    }
});

function clickPubLink(event, obj) {

    event = event || window.event;

    event.preventDefault();

    //get area (abs|bib)
    var area = obj.getAttribute("rel").substr(0, 3);

    //get id
    var id = obj.getAttribute("rel").substr(4);

    var toggle = document.getElementById(area + '-' + id).style.display === 'block';

    // First close all areas.
    if (document.getElementById('abs-' + id)) {
        document.getElementById('abs-' + id).style.display = 'none';
    }
    if (document.getElementById('end-' + id)) {
        document.getElementById('end-' + id).style.display = 'none';
    }
    if (document.getElementById('bib-' + id)) {
        document.getElementById('bib-' + id).style.display = 'none';
    }

    // Show the clicked are.
    if (toggle === false) {
        document.getElementById(area + '-' + id).style.display = 'block';
    }

    // Get content via ajax for bibtex and endnote.
    if (obj.className.indexOf('bibtex') > -1 || obj.className.indexOf('endnote') > -1) {

        loadurl(obj.getAttribute("href"), area + "-" + id);
    }

    return false;
}

function clickAddToMyList(event, obj) {

    event = event || window.event;

    event.preventDefault();
    loadurl(obj.getAttribute("href"), obj.parentNode.getAttribute("id"));
}

/*
Simple Image Trail script- By JavaScriptKit.com
Visit http://www.javascriptkit.com for this script and more
This notice must stay intact
*/
var w = 1;
var h = 1;

if (document.getElementById || document.all) {

   document.write('<div id="pbm_trailimageid"><img id="ttimg" src="../mod/pbm/pix/loading.gif" /></div>');
}

function gettrailobj()
{
   if (document.getElementById)
       return document.getElementById("pbm_trailimageid").style;
   else if (document.all)
       return document.all.trailimagid.style;
}

function truebody()
{
   return (!window.opera && document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
}

function hidetrail()
{
   document.onmousemove = "";
   document.getElementById('ttimg').src = '../mod/pbm/pix/loading.gif';
   gettrailobj().display = "none";
   gettrailobj().left = -1000;
   gettrailobj().top = 0;
}


function showtrail(file)
{
   if (navigator.userAgent.toLowerCase().indexOf('opera') === -1) {
       document.getElementById('ttimg').src = file;
       document.onmousemove = followmouse;
       gettrailobj().display = "block";

       gettrailobj().padding  = "5px";
       gettrailobj().paddingBottom  = "0px";

   }
}

function followmouse(e) {

   if (navigator.userAgent.toLowerCase().indexOf('opera') === -1) {

       var xcoord = 20;
       var ycoord = 20;

       if (typeof e !== "undefined") {
           xcoord += e.pageX;
           ycoord += e.pageY;
       }
       else if (typeof window.event !== "undefined") {
           xcoord += truebody().scrollLeft + event.clientX;
           ycoord += truebody().scrollTop + event.clientY;
       }

       var docwidth = document.all ? truebody().scrollLeft + truebody().clientWidth : pageXOffset + window.innerWidth - 15;
       var docheight = document.all ? Math.max(truebody().scrollHeight, truebody().clientHeight) : Math.max(document.body.offsetHeight, window.innerHeight);

       if (xcoord + w + 3 > docwidth)
           xcoord = xcoord - w - (20 * 2);

       if (ycoord - truebody().scrollTop + h > truebody().clientHeight)
           ycoord = ycoord - h - 20;

       gettrailobj().left = xcoord + "px";
       gettrailobj().top = ycoord + "px";

   }

}