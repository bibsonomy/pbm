<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of interface functions and constants for plugin pbm
 *
 * All the core Moodle functions, needed to allow the plugin to work
 * integrated in Moodle should be placed here.
 * All the pbm specific functions, needed to implement all the module
 * logic, should go to locallib.php. This will help to save some memory when
 * Moodle is performing actions across all modules.
 *
 * @package pbm
 * @author  Florian Fassing
 * @author  Sebastian Böttger
 * @license http://www.gnu.org/licenses/gpl.html GNU GPL v3 or later
 */
require_once('vendor/autoload.php');
require_once('classes/utils/pbm_utils.php');

use AcademicPuma\RestClient\RESTClient;
use AcademicPuma\RestClient\Config;
use AcademicPuma\RestClient\Accessor;
use AcademicPuma\RestClient\Renderer;
use AcademicPuma\RestClient\Model;
use AcademicPuma\RestClient\Config\Sorting;
use AcademicPuma\CiteProc\CiteProc;

defined('MOODLE_INTERNAL') || die();
define('MODULE_NAME', 'pbm');

global $CFG, $PAGE;

$settings = array();

$PAGE->requires->js('/mod/' . MODULE_NAME . '/jquery-1.11.0.min.js', true);
$PAGE->requires->js('/mod/' . MODULE_NAME . '/pbm.js', true);

/**
 * Saves a new instance of pbm into the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @param object $pbm An object from the form in mod_form.php
 * @param mod_pbm_mod_form $mform
 * @return int The id of the newly inserted pbm record
 */
function pbm_add_instance(stdClass $pbm, mod_pbm_mod_form $mform = null) {

    global $DB;

    $pbm->timecreated = time();
    //$pbm->auth = $pbm->auth_group['auth'];

    return $DB->insert_record(MODULE_NAME, $pbm);
}

/**
 * Updates an instance of pbm in the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @param object $pbm An object from the form in mod_form.php
 * @param mod_pbm_mod_form $mform
 * @return boolean Success/Fail
 */
function pbm_update_instance(stdClass $pbm, mod_pbm_mod_form $mform = null) {
    global $DB, $USER;

    // Get the pbm instance data from database to compare if the editing user also is the user
    // who created the activity.
    $tempPbm = $DB->get_record(MODULE_NAME, array('id' => $pbm->instance));

    // Check if the editing user is the same as the user who created the instance.
    if ($tempPbm->user_id == $USER->id) {

        // Manually set to 0 as it doesn't get updated automatically. (Might be a Moodle bug)
        if (!isset($pbm->doc_download_allowed)) {
            $pbm->doc_download_allowed = 0;
        }

        $pbm->timemodified = time();
        $pbm->id = $pbm->instance;

        return $DB->update_record(MODULE_NAME, $pbm);
    }

    return null;
}

/**
 * Removes an instance of the newmodule from the database
 *
 * Given an ID of an instance of this plugin,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @param int $id Id of the plugin instance
 * @return boolean Success/Failure
 */
function pbm_delete_instance($id) {
    global $DB;

    if (!$pbm = $DB->get_record(MODULE_NAME, array('id' => $id))) {
        return false;
    }

    $DB->delete_records(MODULE_NAME, array('id' => $pbm->id));

    return true;
}

/**
 * Returns the information on whether the plugin supports a feature
 *
 * @see plugin_supports() in lib/moodlelib.php
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed true if the feature is supported, null if unknown
 */
function pbm_supports($feature) {
    switch ($feature) {
        case FEATURE_MOD_INTRO:
            return false;
        case FEATURE_SHOW_DESCRIPTION:
            return true;

        default:
            return null;
    }
}

/**
 *
 * @param type $cm
 * @return \cached_cm_info
 */
function pbm_get_coursemodule_info($cm) {
    $info = new cached_cm_info();
    return $info;
}

/**
 * First method to be called when visiting course with pbm instance.
 *
 */
function pbm_cm_info_view(cm_info $cm, $setcontent = true) {
    global $DB, $settings;

    $hasfailed = false;

    try {
        // Get module-settings from database which were defined in mod_form.php.
        $settings = $DB->get_record($cm->name, array('course' => $cm->course, 'id' => $cm->instance));

        // Get the specific api-user and -key with the DB-IDs and the user-id who added the PBM instance.
        $apiuser = $DB->get_field('user_info_data', 'data', array('userid' => $settings->user_id,
            'fieldid' => pbm_utils::mod_pbm_get_db_id('pbm_api_user')));
        $apikey = $DB->get_field('user_info_data', 'data', array('userid' => $settings->user_id,
            'fieldid' => pbm_utils::mod_pbm_get_db_id('pbm_api_key')));
        $apiserver = $DB->get_field('user_info_data', 'data', array('userid' => $settings->user_id,
            'fieldid' => pbm_utils::mod_pbm_get_db_id('pbm_api_server')));
        $accesstokenserialized = $DB->get_field('user_info_data', 'data', array('userid' => $settings->user_id,
            'fieldid' => pbm_utils::mod_pbm_get_db_id('pbm_access_token')));

        $content = '<ul class="bibsonomy_publications" style="list-style-type: none;">';
        $authaccessor = null;

        // Init Accessor.
        if ($settings->auth) { // BasicAuth
            if (filter_var($apiserver, FILTER_VALIDATE_URL) === false) {
                $content .= "<li>". get_string('basic_auth_server_error', MODULE_NAME) ."</li>";
                $hasfailed = true;
                // To prevent the next step to be excecuted.
                $apiserver = null;
            }

            if (!empty($apiuser) && !empty($apikey) && !empty($apiserver)) {
                $authaccessor = new Accessor\BasicAuthAccessor($apiserver, $apiuser, $apikey);
            } else if (!$hasfailed) {
                $content .= "<li>". get_string('basic_auth_error', MODULE_NAME) ."</li>";
                $hasfailed = true;
            }
        } else { // OAuth
            if (!empty($accesstokenserialized)) {

                // Cut off the trailing / (slash) from the url.
                if (substr(get_config(MODULE_NAME, 'default_server'), -1) === '/') {
                    $baseurl = substr(get_config(MODULE_NAME, 'default_server'), 0, -1);
                } else {
                    $baseurl = get_config(MODULE_NAME, 'default_server');
                }

                $authaccessor = new Accessor\OAuthAccessor($baseurl, pbm_utils::unpack_oauth_token($accesstokenserialized),
                        get_config(MODULE_NAME, 'consumer_key'), get_config(MODULE_NAME, 'consumer_secret'));

            } else if (!$hasfailed) {
                $content .= "<li>". get_string('accesstoken_error', MODULE_NAME) ."</li>";
                $hasfailed = true;
            }
        }

        // Init RestClient.
        if (!empty($authaccessor)) {
            $restclient = new RESTClient($authaccessor);
        } else if (!$hasfailed) {
            $content .= "<li>". get_string('accessor_error', MODULE_NAME) ."</li>";
            $hasfailed = true;
        }

        $posts = null;

        // Get posts.
        if (!empty($restclient)) {
            $posts = $restclient->getPosts('bibtex', $settings->source_id_type, $settings->source_id,
                    explode(" ", $settings->pbm_tags))->model();
        } else if (!$hasfailed) {
            $content .= "<li>". get_string('restclient_error', MODULE_NAME) ."</li>";
            $hasfailed = true;
        }

        // Render posts when they could be fetched.
        if ($posts != null) {
            if ($posts->getStart() == 0 && $posts->getEnd() == 0 && !$hasfailed) {
                $content .= "<li>". get_string('resources_error', MODULE_NAME) ."</li>";
                $hasfailed = true;
            } else {

                $content .= pbm_render_publication_list($cm, $posts, $restclient);
                
                // Render a direct link to the publication list.
                $base_uri = $authaccessor->getClient()->getConfig()['base_uri'];
                $base_url = $base_uri->getScheme() . '://' . $base_uri->getHost();
                $content .= '<a href="' . $base_url . '/' .  $settings->source_id_type . '/' . $settings->source_id . '/'
                        .implode("+", explode(" ", $settings->pbm_tags)) . '" '. 'target="_blank">'
                        .get_string('direct_link', MODULE_NAME) . ' ' . $base_url . '.</a>';
            }
        } else if (!$hasfailed) {
            $content .= "<li>". get_string('fetch_pub_error', MODULE_NAME) ."</li>";
            $hasfailed = true;
        }

        if ($setcontent) {
            $cm->set_content('<div style="margin-left:0px;">' . $content . '</ul></div>');
        } else {
            return '<div style="margin-left:0px;">' . $content . '</ul></div>';
        }
    } catch (Exception $e) {
        $cm->set_content($e->getMessage());
    }
}

/**
 * Renders a list of publication specified by the mod_form.php.
 *
 * @param array $posts
 * @return string
 */
function pbm_render_publication_list(cm_info $cm, Model\Posts $posts, RestClient $restclient) {
    global $CFG, $settings;

    $cslrenderer = new Renderer\CSLModelRenderer();

    // Load style and citation processor.
    $citationstyle = CiteProc::loadStyleSheet($settings->citation_style);
    $citeproc = new CiteProc($citationstyle, $settings->csl_language);

    $content = '';
    $loadinggif = '<img alt="loading" src="' . $CFG->wwwroot . '/mod/' . $cm->name . '/pix/loading.gif">';

    // Sort according to the plugin settings.
    $sortOrder = $settings->sort_order === "ascending" ? Sorting::ORDER_ASC : Sorting::ORDER_DESC;
    $posts->sort($settings->sort_by, $sortOrder);

    foreach ($posts as $post) {

        $intrahash = $post->getResource()->getIntraHash();
        $img = pbm_render_preview_img($post, $restclient, $settings);
        $cslitem = $cslrenderer->render($post);

        $download = '';
        if (!empty($post->getDocuments()[0])) {
            // OnMouseOver attribute for the preview pic.
            if($settings->doc_download_allowed) {
                $download = pbm_render_download_link($post, $restclient, $cm);
            }
        }

        $bibtex = pbm_render_bibtex_link($cm, $intrahash);
        $endnote = pbm_render_endnote_link($cm, $intrahash);
        $abstractlink = (strlen($post->getResource()->getBibtexAbstract()) > 0 ? '[<a class="pbm-bib-link abstract" rel="abs-'
                .$intrahash. '" href="#">Abstract</a>] ' : '');
        $abstract = (strlen($post->getResource()->getBibtexAbstract()) > 0 ? '<div class="pbm_format" id="abs-' . $intrahash . '"'
                        . 'style="display: none;">' . $post->getResource()->getBibtexAbstract() . '</div>' : '');

        $addtomypuma = pbm_render_addtomypuma_link($cm, $post);

        // Put the pieces together.
        $content .= '<li>'
                        . $img . $citeproc->render($cslitem) . $addtomypuma .
                        '<div class="pbm-bib-links">' . $download . $bibtex . $endnote . $abstractlink . '</div>'
                        . '<div class="pbm_format" id="bib-' . $intrahash . '" style="display: none;">' . $loadinggif . '</div>'
                        . '<div class="pbm_format" id="end-' . $intrahash . '" style="display: none;">' . $loadinggif . '</div>'
                        . $abstract . '<div class="clearer"></div>'
                    . '</li>';
    }

    return $content;
}

/**
 * Renders the onmouseover attribute for a preview picture of a publication.
 *
 * @param type $doc
 * @param RESTClient $restClient
 * @param type $settings
 * @return type
 */
function pbm_render_mouseover_preview($doc, RESTClient $restClient, $settings) {

    $request = $restClient->getDocumentFile($settings->source_id, $doc->getResource()->getIntraHash(),
                $doc->getDocuments()[0]->getFilename(), Config\DocumentType::LARGE_PREVIEW);

    // OnMouseOver attribute for the preview pic.
    return 'onmouseover="showtrail(\'data:image/jpg;base64,' . base64_encode($request->getQuery()->getBody()) . '\');"';
}

/**
 * Renders a small preview picture next to a publication.
 *
 * @param type $doc
 * @param RESTClient $restClient
 * @param type $settings
 * @return string
 */
function pbm_render_preview_img($doc, RESTClient $restClient, $settings) {

    global $settings;

    if (!empty($doc->getDocuments()[0]) && $settings->doc_download_allowed) {

        $onmouseover = pbm_render_mouseover_preview($doc, $restClient, $settings);

        $request = $restClient->getDocumentFile($settings->source_id, $doc->getResource()->getIntraHash(),
                $doc->getDocuments()[0]->getFilename(), Config\DocumentType::SMALL_PREVIEW);

        $img = '<img class="pbm_preview_pic" src="' . 'data:image/jpg;base64,' . base64_encode($request->getQuery()->getBody())
                . '"' . $onmouseover . ' onmouseout="hidetrail();" />';

    } else {
        $type = 'standard';

        if ($doc->getResource()->hasEntrytype()) {
            $type = $doc->getResource()->getEntrytype();
        }

        $img = '<div class="pbm_placeholder_wrap"><div class="pbm_placeholder pbm_' . $type . '"></div></div>';
    }

    return $img;
}

function pbm_render_download_link($doc, RESTClient $restClient, cm_info $cm) {
    global $settings;

    $request = $restClient->getDocumentFile($settings->source_id, $doc->getResource()->getIntraHash(),
                $doc->getDocuments()[0]->getFilename(), Config\DocumentType::LARGE_PREVIEW);

    $href = "data:image/jpg;base64," . base64_encode($request->getQuery()->getBody());

    return '[<a href="'.$href.'" title="Download Document" download>Download</a>] ';
}

function pbm_render_bibtex_link(cm_info $cm, $intrahash) {
    global $CFG;
    return '[<a class="pbm-bib-link bibtex" rel="bib-' . $intrahash . '" href="' .
                $CFG->wwwroot . '/mod/' . $cm->name . '/classes/pbm_fetch_bibtex.php?course=' . $cm->course . '&instance=' .
                $cm->instance . '&hash=' . $intrahash . '&format=bibtex">Bibtex</a>] ';
}

function pbm_render_endnote_link(cm_info $cm, $intrahash) {
    global $CFG;
    return'[<a class="pbm-bib-link endnote"'
                . 'rel="end-' . $intrahash . '" href="' . $CFG->wwwroot . '/mod/' .
                $cm->name . '/classes/pbm_fetch_bibtex.php?course=' . $cm->course . '&instance=' . $cm->instance . '&hash=' .
                $intrahash . '&format=endnote">Endnote</a>] ';
}

/**
 *
 * @global type $CFG
 * @param cm_info $cm
 * @param \AcademicPuma\RestClient\Model\Post $post
 * @return string
 */
function pbm_render_addtomypuma_link(cm_info $cm, Model\Post $post) {
    global $CFG;

    return '<div class="pbm-add-to-my-list-wrap" id="pbm-'
            . $post->getResource()->getIntraHash() .'" data-pbm="'. $CFG->wwwroot . '/mod/' . $cm->name
            .'/classes/pbm_render_add_button.php?course=' . $cm->course . '&instance=' . $cm->instance .'"></div>';
}
