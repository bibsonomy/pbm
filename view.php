<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints a particular instance of pbm
 *
 * @package pbm
 * @copyright  2014 Florian Fassing
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once(dirname(__FILE__) . '/lib.php');

global $DB, $PAGE;

// Course_module ID, or
// pbm instance ID - it should be named as the first character of the module.
$id = optional_param('id', 0, PARAM_INT);
$p = optional_param('p', 0, PARAM_INT);

if ($id) {
    $cm = get_coursemodule_from_id(MODULE_NAME, $id, 0, false, MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $pbm = $DB->get_record(MODULE_NAME, array('id' => $cm->instance), '*', MUST_EXIST);
} else if ($n) {
    $pbm = $DB->get_record(MODULE_NAME, array('id' => $p), '*', MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $pbm->course), '*', MUST_EXIST);
    $cm = get_coursemodule_from_instance(MODULE_NAME, $pbm->id, $course->id, false, MUST_EXIST);
} else {
    error('You must specify a course_module ID or an instance ID');
}

require_login($course, true, $cm);
$context = context_module::instance($cm->id);

$PAGE->set_url('/mod/pbm/view.php', array('id' => $cm->id));
$PAGE->set_title(format_string($pbm->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($context);

// Output starts here.
echo $OUTPUT->header();

$modinfo = get_fast_modinfo($course);

$cm = $modinfo->get_cm($cm->id);

echo pbm_cm_info_view($cm, false);
// Finish the page.
echo $OUTPUT->footer();
