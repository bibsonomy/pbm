<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines the version of pbm
 *
 * This code fragment is called by moodle_needs_upgrading() and
 * /admin/index.php
 *
 * The version number of the plugin. The format is partially date based with the form YYYYMMDDXX where XX is an incremental
 * counter for the given year (YYYY), month (MM) and date (DD) of the plugin version's release. Every new plugin version must
 * have this number increased in this file, which is detected by Moodle core and the upgrade process is triggered.
 *
 * In case of linear development (i.e. there is just one branch of the plugin maintained), the XX counter should stay
 * with the initial 00 value unless multiple versions are released on the same date.
 *
 * If multiple stable branches of the plugin are maintained, the date part YYYYMMDD should be frozen at the branch
 * forking date and the XX is used for incrementing the value on the given stable branch (allowing up to 100 updates
 * on the given stable branch). The key point is that the version number is always increased both horizontally (within
 * the same stable branch, more recent updates have higher XX than any previous one) and vertically (between any two
 * stable branches, the more recent branch has YYYYMMDD00 higher than the older stable branch). Pay attention to this.
 * It's easy to mess up and hard to fix.
 *
 * @package    pbm
 * @author     Florian Fassing
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

// The current plugin version (Date: YYYYMMDDXX).
$plugin->version = 2018082100; // Current release from 21st August 2018.
$plugin->release = 2014071702; // First released on 17th July 2014.
$plugin->maturity = MATURITY_STABLE;
$plugin->requires = 2015111600; // Requires at least Moodle 3.0
// Period for cron to check this module (secs).
$plugin->cron = 0;
// To check on upgrade, that module sits in correct place.
$plugin->component = 'mod_pbm';
