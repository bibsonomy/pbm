<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The main pbm configuration form.
 *
 * It uses the standard core Moodle formslib. For more info about them, please
 * visit: http://docs.moodle.org/en/Development:lib/formslib.php
 *
 * @package    pbm
 * @copyright  2014 Florian Fassing
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
if (!defined('MOODLE_INTERNAL'))
{
    die('Direct access to this script is forbidden.');
}

require_once($CFG->dirroot . '/course/moodleform_mod.php');

class mod_pbm_mod_form extends moodleform_mod
{

    protected function definition()
    {
        global $USER, $DB;

        $mform = &$this->_form;

        $textattributes = array('size' => '28');

        // SOURCE_ID
        $mform->addElement('select', 'source_id_type', get_string('source_id_type', MODULE_NAME), array('user' => 'User',
            'group' => 'Group'), array('ONCHANGE' => 'checktext(this.value);'));
        $mform->setType('source_id_type', PARAM_TEXT);
        $mform->disabledIf('source_id_type', 'user_id', 'neq', $USER->id);

        $mform->addElement('text', 'source_id', get_string('source_id', MODULE_NAME), $textattributes);
        $mform->setType('source_id', PARAM_TEXT);
        $mform->addRule('source_id', null, 'required', null, 'client');
        $mform->addHelpButton('source_id', 'source_id', MODULE_NAME);
        $mform->disabledIf('source_id', 'user_id', 'neq', $USER->id);

        // Tags
        $mform->addElement('text', 'pbm_tags', get_string('tags', MODULE_NAME), $textattributes);
        $mform->setType('pbm_tags', PARAM_TAGLIST);
        $mform->addHelpButton('pbm_tags', 'tags', MODULE_NAME);
        $mform->disabledIf('pbm_tags', 'user_id', 'neq', $USER->id);

        // doc_download
        $mform->addElement('checkbox', 'doc_download_allowed', get_string('docs_downloadable', MODULE_NAME));
        $mform->addHelpButton('doc_download_allowed', 'docs_downloadable', MODULE_NAME);
        $mform->disabledIf('doc_download_allowed', 'user_id', 'neq', $USER->id);
        $mform->setDefault('doc_download_allowed', 0);

        // LOGIN
        $mform->addElement('header', 'header_auth', get_string('login', MODULE_NAME));

        $radioattributes = null;

        $consumerkey = get_config(MODULE_NAME, 'consumer_key');
        $consumersecret = get_config(MODULE_NAME, 'consumer_secret');

        if (empty($consumerkey) || empty($consumersecret))
        {
            $radioattributes = array('disabled' => 'disabled');
        }

        // auth
        $radioarray = array();
        $radioarray[] = $mform->createElement('radio', 'auth', '', get_string('basicauth', MODULE_NAME), 1);
        $radioarray[] = $mform->createElement('radio', 'auth', '', get_string('oauth', MODULE_NAME), 0, $radioattributes);
        $mform->addGroup($radioarray, 'auth_group', get_string('auth', MODULE_NAME), ' | ', false);
        $mform->setDefault('auth', 1);
        $mform->addHelpButton('auth_group', 'auth', MODULE_NAME);

        // server
        $fieldid = pbm_utils::mod_pbm_get_db_id('pbm_api_server');
        $basicauthserver = $DB->get_field('user_info_data', 'data', array('userid' => $USER->id, 'fieldid' => $fieldid));

        // If no BasicAuth server was found set an error message.
        if ($basicauthserver === false || empty($basicauthserver))
        {
            $basicauthserver = get_string('basic_auth_server_error', MODULE_NAME);
        }

        $mform->addElement('static', get_string('server_description', MODULE_NAME), get_string('server', MODULE_NAME), $basicauthserver . " <b>|</b> " . get_config(MODULE_NAME, 'default_server'));

        $mform->closeHeaderBefore('header_extended');

        // LAYOUT.
        $mform->addElement('header', 'header_extended', get_string('layout', MODULE_NAME));

        $styles = explode(',', get_config(MODULE_NAME, 'available_csl_files'));

        // Citation_style.
        $mform->addElement('select', 'citation_style', get_string('citation_style', MODULE_NAME), array_combine($styles, $styles), array('ONCHANGE' => 'checktext(this.value);'));
        $mform->addHelpButton('citation_style', 'citation_style', MODULE_NAME);
        $mform->disabledIf('citation_style', 'user_id', 'neq', $USER->id);

        // csl language
        $languages = array();
        if (($dh = opendir(realpath(dirname(__FILE__)) . '/vendor/academicpuma/locales')) !== false)
        {
            while (($file = readdir($dh)) !== false) {
                if (pathinfo($file, PATHINFO_EXTENSION) === "xml")
                {
                    $keyAndValue = substr(substr($file, 0, -4), -5, 5);
                    $languages[$keyAndValue] = $keyAndValue;
                }
            }
            closedir($dh);
        }

        asort($languages, SORT_STRING);

        $mform->addElement('select', 'csl_language', get_string('csl_language', MODULE_NAME), $languages);
        $mform->addHelpButton('csl_language', 'csl_language', MODULE_NAME);
        $mform->disabledIf('csl_language', 'user_id', 'neq', $USER->id);

        // SORTING
        $mform->addElement('header', 'sort', get_string('sort', MODULE_NAME));

        $sortCriterions = array(
            get_string('year', MODULE_NAME) => 'year',
            get_string('title', MODULE_NAME) => 'title',
            get_string('author', MODULE_NAME) => 'author'
        );

        // Sort_by
        $mform->addElement('select', 'sort_by', get_string('sort_by', MODULE_NAME), $sortCriterions);
        $mform->disabledIf('sort_by', 'user_id', 'neq', $USER->id);

        // Sort_order
        $mform->addElement('select', 'sort_order', get_string('sort_order', MODULE_NAME), array(get_string('asc', MODULE_NAME) =>
            get_string('asc', MODULE_NAME), get_string('desc', MODULE_NAME) => get_string('desc', MODULE_NAME)));
        $mform->disabledIf('sort_order', 'user_id', 'neq', $USER->id);

        $userid = empty($this->current->id) ? $USER->id : $this->current->id;

        // HIDDEN
        $mform->addElement('hidden', 'user_id', $userid);
        $mform->setType('user_id', PARAM_INT);

        $this->standard_coursemodule_elements();
        $this->add_action_buttons();
    }

}
