<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once('../vendor/autoload.php');
require_once('../../../config.php');
require_once('utils/pbm_utils.php');

use AcademicPuma\RestClient\RESTClient;
use AcademicPuma\RestClient\Accessor;

global $DB, $USER;

header( 'Content-type: text/html; charset=utf-8' );

// Get module-settings from database which were defined in mod_form.php.
$settings = $DB->get_record('pbm', array('course' => pbm_utils::filter_get('course'), 'id' => pbm_utils::filter_get('instance')));

// Get a specific api-user and -key from the DB-ID and the user-id of the current user.
$apiuser = $DB->get_field('user_info_data', 'data', array('userid' => $USER->id,
    'fieldid' => pbm_utils::mod_pbm_get_db_id('pbm_api_user')));
$apikey = $DB->get_field('user_info_data', 'data', array('userid' => $USER->id,
    'fieldid' => pbm_utils::mod_pbm_get_db_id('pbm_api_key')));
$accesstokenserialized = $DB->get_field('user_info_data', 'data', array('userid' => $USER->id,
    'fieldid' => pbm_utils::mod_pbm_get_db_id('pbm_access_token')));

$authaccessor = null;
$user = null;

// Init Accessor. Prefer BasicAuth, if not available try OAuth. If no auth credentials available don't show the add button.
if (!empty($apiuser) && !empty($apikey)) {

    $fieldid = pbm_utils::mod_pbm_get_db_id('pbm_api_server');
    $basicauthserver = $DB->get_field('user_info_data', 'data', array('userid' => $USER->id, 'fieldid' => $fieldid));

    $authaccessor = new Accessor\BasicAuthAccessor($basicauthserver, $apiuser, $apikey);
    $user = $apiuser;

} else if (!empty($accesstokenserialized)) {
    $accesstoken = pbm_utils::unpack_oauth_token($accesstokenserialized);
    $authaccessor = new Accessor\OAuthAccessor(get_config('pbm', 'default_server'), $accesstoken, get_config('pbm', 'consumer_key'),
            get_config('pbm', 'consumer_secret'));
    $user = $accesstoken->getUserId();
} else {
    pbm_render_info_button();
}

// Init RestClient.
if (!empty($authaccessor)) {
    $restclient = new RESTClient($authaccessor);
} else {
    pbm_render_info_button();
}

// Check if user has post in his own list, if not render the add button.
if (!empty($restclient)) {
    try {
        $post = $restclient->getPostDetails($user, pbm_utils::filter_get("intrahash"))->model();
        echo '<div id="'. pbm_utils::filter_get("intrahash") .'-tick" class="pbm-tick"></div>';
    } catch (Exception $ex) {
        pbm_render_button($ex);
    }
} else {
    pbm_render_info_button();
}

/**
 * Render an add button if the post
 * is not in the users list.
 *
 * @global type $CFG
 * @param Exception $ex
 */
function pbm_render_button(Exception $ex) {

    global $CFG;

    // If a post is not existent in the users list the exception code is 404.
    if ($ex->getCode() == 404) {
        $intrahash = pbm_utils::filter_get("intrahash");

        $href = $CFG->wwwroot . '/mod/pbm/classes/pbm_add_to_my_list.php?course='
                . pbm_utils::filter_get("course") . '&instance=' . pbm_utils::filter_get("instance") .'&intrahash='. $intrahash;

        echo '<a id="'. $intrahash .'" class="pbm-add-to-my-list" href="'. $href .'" onclick="clickAddToMyList(event, this)">'
                . '<div class="pbm-plus"></div></a>';
    } else {
        pbm_render_info_button();
    }
}

/**
 * Render a info button when the user has
 * not provided any authentication data.
 *
 * @global type $OUTPUT
 * @global type $settings
 */
function pbm_render_info_button() {

    global $OUTPUT, $settings;

    require_login($settings->course);
    $helpicon = new help_icon('addtomypuma', 'pbm');
    echo $OUTPUT->render($helpicon);

    die;
}
