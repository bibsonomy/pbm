<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Util functions for the pbm plugin.
 *
 * @package pbm
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @author Florian Fassing
 */
class pbm_utils {

    public static function filter_get($varname) {
        return filter_input(INPUT_GET, $varname, FILTER_DEFAULT);
    }

    public static function filter_ses($varname) {
        return filter_input(INPUT_SESSION, $varname, FILTER_DEFAULT);
    }

    public static function mod_pbm_add_db_field($DB, $fieldstable, $shortname, $name, $type, $categoryid, $description = '', $visible = 1, $fieldsize = null, $maxlength = null, $locked = 0) {

        if (!$DB->record_exists($fieldstable, array('shortname' => $shortname))) {
            $userfield = new stdClass();
            $userfield->shortname = $shortname;
            $userfield->name = $name;
            $userfield->datatype = $type;
            $userfield->categoryid = $categoryid;
            $userfield->description = $description;
            $userfield->sortorder = 1;
            $userfield->required = 0;
            $userfield->locked = $locked;
            $userfield->visible = $visible;
            $userfield->forceunique = 0;
            $userfield->signup = 1;
            $userfield->defaultdata = '';
            // Size of the field.
            $userfield->param1 = $fieldsize;
            // Max length of the field.
            $userfield->param2 = $maxlength;
            // Password field or not?
            $userfield->param3 = 0;
            // Text type contains link?
            $userfield->param4 = null;
            // Text type contains link target e.g. _blank?
            $userfield->param5 = null;
            $DB->insert_record($fieldstable, $userfield, false);
        }
    }

    public static function mod_pbm_get_db_id ($shortname) {

        global $DB;

        return $DB->get_field('user_info_field', 'id', array('shortname' => $shortname));
    }

    public static function unpack_oauth_token($accesstokenserialized) {
        return unserialize(base64_decode($accesstokenserialized));
    }

    public static function pack_oauth_token($accesstoken) {
        return base64_encode(serialize($accesstoken));
    }
}
