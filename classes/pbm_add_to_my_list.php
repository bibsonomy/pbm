<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once('../vendor/autoload.php');
require_once('../../../config.php');
require_once('utils/pbm_utils.php');

use AcademicPuma\RestClient\Model;
use AcademicPuma\RestClient\RESTClient;
use AcademicPuma\RestClient\Accessor;
use AcademicPuma\RestClient\Config;

global $DB, $USER;

// Get module-settings from database which were defined in mod_form.php.
$settings = $DB->get_record('pbm', array('course' => pbm_utils::filter_get('course'), 'id' => pbm_utils::filter_get('instance')));

// Get the api-user and -key from the DB in order to create the new publication.
$apiuser = $DB->get_field('user_info_data', 'data', array('userid' => $USER->id,
    'fieldid' => pbm_utils::mod_pbm_get_db_id('pbm_api_user')));
$apikey = $DB->get_field('user_info_data', 'data', array('userid' => $USER->id,
    'fieldid' => pbm_utils::mod_pbm_get_db_id('pbm_api_key')));
$accesstokenserialized = $DB->get_field('user_info_data', 'data', array('userid' => $USER->id,
    'fieldid' => pbm_utils::mod_pbm_get_db_id('pbm_access_token')));

// Get the api-user and -key from the DB in order to get the publication which will then be created.
$apiuserget = $DB->get_field('user_info_data', 'data', array('userid' => $settings->user_id,
    'fieldid' => pbm_utils::mod_pbm_get_db_id('pbm_api_user')));
$apikeyget = $DB->get_field('user_info_data', 'data', array('userid' => $settings->user_id,
    'fieldid' => pbm_utils::mod_pbm_get_db_id('pbm_api_key')));
$accesstokenserializedget = $DB->get_field('user_info_data', 'data', array('userid' => $settings->user_id,
    'fieldid' => pbm_utils::mod_pbm_get_db_id('pbm_access_token')));

// The accessor to create the publication.
$authaccessor = null;
// The accessor to first fetch the publication which will then be created.
$authaccessorget = null;
// true = basicAuth, false = OAuth
$basicauth = true;

 $fieldid = pbm_utils::mod_pbm_get_db_id('pbm_api_server');

// Init Accessor. Prefer BasicAuth, if not available try OAuth. If no auth credentials available don't show the add button.
if (!empty($apiuser) && !empty($apikey)) {

    $basicauthserver = $DB->get_field('user_info_data', 'data', array('userid' => $USER->id, 'fieldid' => $fieldid));

    $authaccessor = new Accessor\BasicAuthAccessor($basicauthserver, $apiuser, $apikey);
} else if (!empty($accesstokenserialized)) {

    $authaccessor = new Accessor\OAuthAccessor(get_config('pbm', 'default_server'), pbm_utils::unpack_oauth_token($accesstokenserialized),
            get_config('pbm', 'consumer_key'), get_config('pbm', 'consumer_secret'));
    $basicauth = false;
}

// Init AccessorGet.
if (!empty($apiuserget) && !empty($apikeyget)) {

    $basicauthserver = $DB->get_field('user_info_data', 'data', array('userid' => $settings->user_id, 'fieldid' => $fieldid));

    $authaccessorget = new Accessor\BasicAuthAccessor($basicauthserver, $apiuserget, $apikeyget);
} else if (!empty($accesstokenserializedget)) {

    $authaccessorget = new Accessor\OAuthAccessor(get_config('pbm', 'default_server'), pbm_utils::unpack_oauth_token($accesstokenserializedget),
            get_config('pbm', 'consumer_key'), get_config('pbm', 'consumer_secret'));
}

$restclient = null;
$restclientget = null;

// Init RestClient.
if (!empty($authaccessor) && !empty($authaccessorget)) {
    $restclient = new RESTClient($authaccessor);
    $restclientget = new RESTClient($authaccessorget);
} else {
    print get_string(restclient_error, 'pbm');
}

if (!empty($restclient) && !empty($restclientget)) {

    $post = null;

    if ($settings->source_id_type === 'group') {
        $posts = $restclientget->getPosts(Config\Resourcetype::BIBTEX, Config\Grouping::GROUPING,
                Config\Grouping::GROUPING_VALUE_ALL, [], '2' . pbm_utils::filter_get("intrahash"))->model();
        $post = $posts[0];
    } else {
        $post = $restclientget->getPostDetails($settings->source_id, pbm_utils::filter_get("intrahash"))->model();
    }

    // Get the correct apiuser (oauth has its own api-user).
    $authuser = $basicauth ? $apiuser : pbm_utils::unpack_oauth_token($accesstokenserialized)->getUserId();

    // Init user.
    $user = new Model\User();
    $user->setName($authuser);
    $post->setUser($user);

    $restclient->createPosts($post, $authuser);

    echo '<div class="pbm-tick"></div>';
}

