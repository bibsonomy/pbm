<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once(__DIR__ . '/../vendor/autoload.php');
require_once('utils/pbm_utils.php');

use AcademicPuma\OAuth;

/**
 * Observer holds functions called by events. (Hooks)
 *
 * @author Florian
 */
class mod_pbm_observer {

    public static function get_access_key(core\event\user_updated $event) {

        global $DB;

        $oauthconnect = $DB->get_field('user_info_data', 'data', array('userid' => $event->get_data()['userid'],
            'fieldid' => pbm_utils::mod_pbm_get_db_id('pbm_oauth_connect')));

        $accesstoken = $DB->get_field('user_info_data', 'data', array('userid' => $event->get_data()['userid'],
            'fieldid' => pbm_utils::mod_pbm_get_db_id('pbm_access_token')));

        // User wants to link his/her account.
        if ($oauthconnect && empty($accesstoken)) {

            $callbackurl = new moodle_url('/mod/pbm/classes/oauth_callback.php');

            $adapter = new OAuth\OAuthAdapter([
                'consumerKey'       => get_config('pbm', 'consumer_key'),
                'consumerSecret'    => get_config('pbm', 'consumer_secret'),
                'callbackUrl'       => $callbackurl->out(),
                'baseUrl'           => get_config('pbm', 'default_server') . '/'
            ]);

            try {
                $requestToken = $adapter->getRequestToken(); //get Request Token
                $_SESSION['REQUEST_TOKEN'] = serialize($requestToken); //save Request Token in the session
                $_SESSION['USER_ID'] = $event->get_data()['userid']; // is needed to save accesstoken into db (see oauth_callback).

            } catch(\Exception $e) {
                error_log($e->getMessage()." in ".$e->getFile()." on line ". $e->getLine());
            }

            $adapter->redirect($requestToken); //redirect to PUMA/BibSonomy to verify user authorization
            die;
        } else if (!$oauthconnect && !empty($accesstoken)) {
            // Delete accesstoken if user cancels oauth connection.
            $accesstoken = $DB->set_field('user_info_data', 'data', '', array('userid' => $event->get_data()['userid'],
                    'fieldid' => pbm_utils::mod_pbm_get_db_id('pbm_access_token')));
        }
    }
}
