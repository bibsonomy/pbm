<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once('../vendor/autoload.php');
require_once('../../../config.php');
require_once('utils/pbm_utils.php');

use AcademicPuma\RestClient\RESTClient;
use AcademicPuma\RestClient\Accessor;

global $DB;

// Get module-settings from database which were defined in mod_form.php.
$settings = $DB->get_record('pbm', array('course' => pbm_utils::filter_get('course'), 'id' => pbm_utils::filter_get('instance')));

// Get the specific api-user and -key from the DB and the user-id who added the PBM instance.
$apiuser = $DB->get_field('user_info_data', 'data', array('userid' => $settings->user_id,
    'fieldid' => pbm_utils::mod_pbm_get_db_id('pbm_api_user')));
$apikey = $DB->get_field('user_info_data', 'data', array('userid' => $settings->user_id,
    'fieldid' => pbm_utils::mod_pbm_get_db_id('pbm_api_key')));
$accesstokenserialized = $DB->get_field('user_info_data', 'data', array('userid' => $settings->user_id,
    'fieldid' => pbm_utils::mod_pbm_get_db_id('pbm_access_token')));

// Init Accessor.
if ($settings->auth) {

    $fieldid = pbm_utils::mod_pbm_get_db_id('pbm_api_server');
    $basicauthserver = $DB->get_field('user_info_data', 'data', array('userid' => $settings->user_id, 'fieldid' => $fieldid));

    $authaccessor = new Accessor\BasicAuthAccessor($basicauthserver, $apiuser, $apikey);
} else {

    if (!empty($accesstokenserialized)) {
        $authaccessor = new Accessor\OAuthAccessor(get_config('pbm', 'default_server'), unserialize($accesstokenserialized),
            get_config('pbm', 'consumer_key'), get_config('pbm', 'consumer_secret'));
    } else {
        print get_string('accesstoken_error', 'pbm');
    }
}

// Init RestClient.
$restClient = new RESTClient($authaccessor);

try {
    $bibtex = $restClient->getPosts('bibtex', $settings->source_id_type, $settings->source_id, [],
        '', 'intrahash:' . pbm_utils::filter_get('hash'), pbm_utils::filter_get('format'))->getQuery()->getBody();
} catch (\AcademicPuma\RestClient\Exceptions\UnsupportedOperationException $e) {
    print $e;
}


print '<textarea rows="10" style="width:100%;">' . $bibtex . '</textarea>';
die;
