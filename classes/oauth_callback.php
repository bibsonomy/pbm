<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once('../vendor/autoload.php');
require_once('../../../config.php');
require_once('utils/pbm_utils.php');

use \AcademicPuma\OAuth\OAuthAdapter;

global $DB;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// Get the RequestToken from session
$requesttoken = unserialize($_SESSION['REQUEST_TOKEN']);

$adapter = new OAuthAdapter([
   'consumerKey'       => get_config('pbm', 'consumer_key'),
   'consumerSecret'    => get_config('pbm', 'consumer_secret'),
   'callbackUrl'       => '',
   'baseUrl'           => get_config('pbm', 'default_server')
]);

$accesstoken = $adapter->getAccessToken($requesttoken); //fetch Access Token

$table = 'user_info_data';

// Create record for new access token.
$record = new stdClass();
$record->userid = $_SESSION['USER_ID'];
$record->fieldid = pbm_utils::mod_pbm_get_db_id('pbm_access_token');
$record->data = pbm_utils::pack_oauth_token($accesstoken);


$recordExists = $DB->record_exists($table, array('userid' => $record->userid, 'fieldid' => $record->fieldid));

if ($recordExists) {

    $DB->set_field($table, 'data', $record->data, array('userid' => $record->userid, 'fieldid' => $record->fieldid));
} else {
    // Insert the serialized access token.
    $DB->insert_record($table, $record);
}

$backtomoodle = new moodle_url('/user/profile.php');

header('Location: ' . $backtomoodle->out());